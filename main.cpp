#include <iostream>
#include <list>
#include <locale.h>
#include <ctime>
#include <Windows.h>

#define clear system("cls||clear");

typedef struct Processo
{
    int pid;
    int prioridade;
    int tempoExecucao = 0;
    int tempoRestante = 0;
    int tempoEspera = 0;
    int tempoResposta = 0;
    int status = 2;
} processo;

using std::cin;
using std::cout;
using std::endl;
using std::list;

void mostraProcessos(list<processo> processos);
bool comparaTempoExecucao(processo p1, processo p2);
void shortestJobFirst();
void firstComeFirstServed();
void calculaEstatisticas(list<processo> terminados);

list<processo> processos;

void menu()
{ // Menu
    cout << " _______________________________________________________\n|\t Algoritmos de Escalonamento\t\t\t|\n";
    cout << "|        Escolha algum algoritmo de escalonamento\t|\n|\t\t\t\t\t\t\t|\n";
    cout << "|\t 1 - First-Come First-Served\t\t\t|\n";
    cout << "|\t 2 - Shortest Job First\t\t\t\t|\n";
    cout << "|_______________________________________________________|\n";
}

int main()
{

    int escolha;
    do
    {
        clear;
        menu();
        cout << "Digite o algoritmo de escalonamento que deseja: ";
        cin >> escolha;
        switch (escolha)
        {
        case 1:
            // implementa First-Come First-Served
            firstComeFirstServed();
            break;
        case 2:
            // implementa Shortest Job First
            shortestJobFirst();
            break;
        default:
            cout << "Digite um número correto!";
        }
    } while (escolha != 1 && escolha != 2 && escolha != 3);
}

list<processo> adicionaProcessos(list<processo> processos)
{ // método que recebe as informações dos processos
    int quantidade, i = 1;
    bool verificaPidExistente;
    processo p;
    cout << "Digite o número de processos: ";
    cin >> quantidade;
    while (quantidade > 0)
    {
        clear;
        cout << "\nInforme as informações do processo " << i << endl;
        do
        {
            cout << "\nInforme o pid: ";
            cin >> p.pid;
            verificaPidExistente = false;
            for (list<processo>::iterator iterator = processos.begin(); iterator != processos.end(); iterator++)
            {
                if (iterator->pid == p.pid)
                {
                    cout << "Pid já existe!" << endl;
                    verificaPidExistente = true;
                }
            }
        } while (verificaPidExistente);
        cout << "\nInforme a prioridade do processo: ";
        cin >> p.prioridade;
        cout << "\nInforme o tempo de execução do processo: ";
        cin >> p.tempoExecucao;
        p.tempoRestante = p.tempoExecucao;
        processos.push_back(p);
        i++;
        quantidade--;
    }
    return processos;
}

void mostraProcessos(list<processo> processos, list<processo> terminados)
{
    clear;
    if (processos.empty())
    {
        cout << "Todos processos executados com sucesso!" << endl;
    }
    else
    {
        cout << "Processo em andamento: " << endl;
        cout << endl;
    }
    for (list<processo>::iterator iterator = processos.begin(); iterator != processos.end(); iterator++)
    {
        cout << "Id do processo: " << iterator->pid << endl;
        cout << "Prioridade: " << iterator->prioridade << endl;
        cout << "Tempo de Execução: " << iterator->tempoExecucao << " seg" << endl;
        cout << "Tempo Restante: " << iterator->tempoRestante << " seg" << endl;
        cout << "Tempo de Espera: " << iterator->tempoEspera << " seg" << endl;
        cout << "Tempo de Resposta: " << iterator->tempoResposta << " seg" << endl;
        cout << "Status: ";
        switch (iterator->status)
        {
        case 1:
            cout << "Processo em execução" << endl;
            break;
        case 2:
            cout << "Processo em espera" << endl;
            break;
        case 3:
            cout << "Processo finalizado" << endl;
            break;
        case 4:
            cout << "Processo não iniciado" << endl;
            break;
        }
        cout << endl;
    }

    cout << "Processos finalizados: " << endl;
    cout << endl;
    for (list<processo>::iterator iterator = terminados.begin(); iterator != terminados.end(); iterator++)
    {
        iterator->tempoRestante = 0;
        cout << "Id do processo: " << iterator->pid << endl;
        cout << "Prioridade: " << iterator->prioridade << endl;
        cout << "Tempo de execução: " << iterator->tempoExecucao << " seg" << endl;
        cout << "Tempo Restante: " << iterator->tempoRestante << " seg" << endl;
        cout << "Tempo de Espera: " << iterator->tempoEspera << " seg" << endl;
        cout << "Tempo de Resposta: " << iterator->tempoResposta << " seg" << endl;
        cout << "Status: ";
        switch (iterator->status)
        {
        case 1:
            cout << "Processo em execução" << endl;
            break;
        case 2:
            cout << "Processo em espera" << endl;
            break;
        case 3:
            cout << "Processo finalizado" << endl;
            break;
        }
        cout << endl;
    }
}

void shortestJobFirst()
{
    list<processo> terminados;
    int finalizado = 0;
    processos = adicionaProcessos(processos);
    processos.sort(comparaTempoExecucao);
    do
    {
        list<processo>::iterator iterator = processos.begin();
        iterator->status = 1;
        for (list<processo>::iterator iterator = processos.begin(); iterator != processos.end(); iterator++)
        {
            if (iterator->status == 2)
            {
                iterator->tempoEspera += 1;
                iterator->tempoResposta += 1;
            }
        }
        mostraProcessos(processos, terminados);
        Sleep(1000);
        iterator->tempoRestante -= 1;
        if (iterator->tempoRestante == 0)
        {
            finalizado = 1;
            iterator->status = 3;
            terminados.push_back(*iterator);
            iterator = processos.erase(iterator);
            list<processo>::iterator iterator = processos.begin();
        }
    } while (finalizado == 0 || processos.size() > 0);

    mostraProcessos(processos, terminados);

    calculaEstatisticas(terminados);
}

void firstComeFirstServed()
{
    list<processo> terminados;
    int finalizado = 0;
    processos = adicionaProcessos(processos);
    do
    {
        list<processo>::iterator iterator = processos.begin();
        iterator->status = 1;
        for (list<processo>::iterator iterator = processos.begin(); iterator != processos.end(); iterator++)
        {
            if (iterator->status == 2)
            {
                iterator->tempoEspera += 1;
                iterator->tempoResposta += 1;
            }
        }
        mostraProcessos(processos, terminados);
        Sleep(1000);
        iterator->tempoRestante -= 1;
        if (iterator->tempoRestante == 0)
        {
            finalizado = 1;
            iterator->status = 3;
            terminados.push_back(*iterator);
            iterator = processos.erase(iterator);
            list<processo>::iterator iterator = processos.begin();
        }
    } while (finalizado == 0 || processos.size() > 0);

    mostraProcessos(processos, terminados);

    calculaEstatisticas(terminados);
}

void calculaEstatisticas(list<processo> terminados)
{
    int somaTempoEspera = 0;
    int somaTempoResposta = 0;
    int somaTempoExecucao = 0;
    list<processo>::iterator iterator = terminados.begin();
    int menorTempoExecucao = iterator->tempoExecucao;
    int menorTempoEspera = iterator->tempoEspera;
    int menorTempoResposta = iterator->tempoResposta;
    int maiorTempoExecucao = iterator->tempoExecucao;
    int maiorTempoEspera = iterator->tempoEspera;
    int maiorTempoResposta = iterator->tempoResposta;

    for (list<processo>::iterator iterator = terminados.begin(); iterator != terminados.end(); iterator++)
    {

        somaTempoEspera += iterator->tempoEspera;
        somaTempoResposta += iterator->tempoResposta;
        somaTempoExecucao += iterator->tempoExecucao;

        // Verificar menor tempo de execução
        if (iterator->tempoExecucao < menorTempoExecucao)
        {
            menorTempoExecucao = iterator->tempoExecucao;
        }
        // Verificar menor tempo de espera
        if (iterator->tempoEspera < menorTempoEspera)
        {
            menorTempoEspera = iterator->tempoEspera;
        }
        // Verificar menor tempo de resposta
        if (iterator->tempoResposta < menorTempoResposta)
        {
            menorTempoResposta = iterator->tempoResposta;
        }
        // Verificar maior tempo de execução
        if (iterator->tempoExecucao > maiorTempoExecucao)
        {
            maiorTempoExecucao = iterator->tempoExecucao;
        }
        // Verificar maior tempo de espera
        if (iterator->tempoEspera > maiorTempoEspera)
        {
            maiorTempoEspera = iterator->tempoEspera;
        }
        // Verificar maior tempo de resposta
        if (iterator->tempoResposta > maiorTempoResposta)
        {
            maiorTempoResposta = iterator->tempoResposta;
        }
    }

    cout << endl;
    cout << endl;
    cout << "Média de tempo de espera: " << somaTempoEspera / terminados.size() << " seg" << endl;
    cout << "Média de tempo de resposta: " << somaTempoResposta / terminados.size() << " seg" << endl;
    cout << "Média de tempo de execução: " << somaTempoExecucao / terminados.size() << " seg" << endl
         << endl;
    cout << "Turnaround Time: " << somaTempoExecucao << " seg" << endl
         << endl;
    cout << "Menor tempo de execução: " << menorTempoExecucao << " seg" << endl;
    cout << "Menor tempo de espera: " << menorTempoEspera << " seg" << endl;
    cout << "Menor tempo de resposta: " << menorTempoResposta << " seg" << endl
         << endl;
    cout << "Maior tempo de execução: " << maiorTempoExecucao << " seg" << endl;
    cout << "Maior tempo de espera: " << maiorTempoEspera << " seg" << endl;
    cout << "Maior tempo de resposta: " << maiorTempoResposta << " seg" << endl;
    cout << endl;
}

bool comparaTempoExecucao(processo p1, processo p2)
{
    if (p1.tempoExecucao < p2.tempoExecucao)
    {
        return true;
    }
    else
    {
        return false;
    }
}
